import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:test_task_imanov/app/app.dart';
import 'package:test_task_imanov/data/_data.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  /// Orientation Lock for vertical screen
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);

  final dio = Dio(); // Provide a dio instance
  final client = RestClient(dio);

  final mainRepository = MainRepository(client: client);
  runApp(
    App(
      mainRepository: mainRepository,
    ),
  );
}
