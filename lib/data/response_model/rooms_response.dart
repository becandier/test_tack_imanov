// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:test_task_imanov/data/_data.dart';

part 'rooms_response.g.dart';
part 'rooms_response.freezed.dart';

@freezed
class RoomResponseM with _$RoomResponseM {
  const factory RoomResponseM({
    List<RoomM?>? rooms,
  }) = _RoomResponseM;
  const RoomResponseM._();

  factory RoomResponseM.fromJson(Map<String, dynamic> json) =>
      _$RoomResponseMFromJson(json);
}
