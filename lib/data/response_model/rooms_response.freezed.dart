// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'rooms_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

RoomResponseM _$RoomResponseMFromJson(Map<String, dynamic> json) {
  return _RoomResponseM.fromJson(json);
}

/// @nodoc
mixin _$RoomResponseM {
  List<RoomM?>? get rooms => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RoomResponseMCopyWith<RoomResponseM> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RoomResponseMCopyWith<$Res> {
  factory $RoomResponseMCopyWith(
          RoomResponseM value, $Res Function(RoomResponseM) then) =
      _$RoomResponseMCopyWithImpl<$Res, RoomResponseM>;
  @useResult
  $Res call({List<RoomM?>? rooms});
}

/// @nodoc
class _$RoomResponseMCopyWithImpl<$Res, $Val extends RoomResponseM>
    implements $RoomResponseMCopyWith<$Res> {
  _$RoomResponseMCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? rooms = freezed,
  }) {
    return _then(_value.copyWith(
      rooms: freezed == rooms
          ? _value.rooms
          : rooms // ignore: cast_nullable_to_non_nullable
              as List<RoomM?>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$RoomResponseMImplCopyWith<$Res>
    implements $RoomResponseMCopyWith<$Res> {
  factory _$$RoomResponseMImplCopyWith(
          _$RoomResponseMImpl value, $Res Function(_$RoomResponseMImpl) then) =
      __$$RoomResponseMImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<RoomM?>? rooms});
}

/// @nodoc
class __$$RoomResponseMImplCopyWithImpl<$Res>
    extends _$RoomResponseMCopyWithImpl<$Res, _$RoomResponseMImpl>
    implements _$$RoomResponseMImplCopyWith<$Res> {
  __$$RoomResponseMImplCopyWithImpl(
      _$RoomResponseMImpl _value, $Res Function(_$RoomResponseMImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? rooms = freezed,
  }) {
    return _then(_$RoomResponseMImpl(
      rooms: freezed == rooms
          ? _value._rooms
          : rooms // ignore: cast_nullable_to_non_nullable
              as List<RoomM?>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$RoomResponseMImpl extends _RoomResponseM {
  const _$RoomResponseMImpl({final List<RoomM?>? rooms})
      : _rooms = rooms,
        super._();

  factory _$RoomResponseMImpl.fromJson(Map<String, dynamic> json) =>
      _$$RoomResponseMImplFromJson(json);

  final List<RoomM?>? _rooms;
  @override
  List<RoomM?>? get rooms {
    final value = _rooms;
    if (value == null) return null;
    if (_rooms is EqualUnmodifiableListView) return _rooms;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'RoomResponseM(rooms: $rooms)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RoomResponseMImpl &&
            const DeepCollectionEquality().equals(other._rooms, _rooms));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_rooms));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RoomResponseMImplCopyWith<_$RoomResponseMImpl> get copyWith =>
      __$$RoomResponseMImplCopyWithImpl<_$RoomResponseMImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$RoomResponseMImplToJson(
      this,
    );
  }
}

abstract class _RoomResponseM extends RoomResponseM {
  const factory _RoomResponseM({final List<RoomM?>? rooms}) =
      _$RoomResponseMImpl;
  const _RoomResponseM._() : super._();

  factory _RoomResponseM.fromJson(Map<String, dynamic> json) =
      _$RoomResponseMImpl.fromJson;

  @override
  List<RoomM?>? get rooms;
  @override
  @JsonKey(ignore: true)
  _$$RoomResponseMImplCopyWith<_$RoomResponseMImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
