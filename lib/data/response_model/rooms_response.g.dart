// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rooms_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$RoomResponseMImpl _$$RoomResponseMImplFromJson(Map<String, dynamic> json) =>
    _$RoomResponseMImpl(
      rooms: (json['rooms'] as List<dynamic>?)
          ?.map((e) =>
              e == null ? null : RoomM.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$RoomResponseMImplToJson(_$RoomResponseMImpl instance) =>
    <String, dynamic>{
      'rooms': instance.rooms?.map((e) => e?.toJson()).toList(),
    };
