import 'package:test_task_imanov/data/_data.dart';

class MainRepository {
  MainRepository({required this.client});
  final RestClient client;

  ///Get hotel
  Future<HotelM?> getHotel() => client.getHotel();

  ///Get list of rooms
  Future<RoomResponseM?> getRooms() => client.getRooms();

  ///Get info about reservation
  Future<ReservationInfoM?> getReservationInfo() => client.getReservationInfo();
}
