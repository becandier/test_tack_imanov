// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'reservation_info.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ReservationInfoM _$ReservationInfoMFromJson(Map<String, dynamic> json) {
  return _ReservationInfoM.fromJson(json);
}

/// @nodoc
mixin _$ReservationInfoM {
  int? get id => throw _privateConstructorUsedError;
  @JsonKey(name: 'hotel_name')
  String? get hotelName => throw _privateConstructorUsedError;
  @JsonKey(name: 'hotel_adress')
  String? get hotelAdress => throw _privateConstructorUsedError;
  int? get horating => throw _privateConstructorUsedError;
  @JsonKey(name: 'rating_name')
  String? get ratingName => throw _privateConstructorUsedError;
  String? get departure => throw _privateConstructorUsedError;
  @JsonKey(name: 'arrival_country')
  String? get arrivalCountry => throw _privateConstructorUsedError;
  @JsonKey(name: 'tour_date_start')
  String? get tourDateStart => throw _privateConstructorUsedError;
  @JsonKey(name: 'tour_date_stop')
  String? get tourDateStop => throw _privateConstructorUsedError;
  @JsonKey(name: 'number_of_nights')
  int? get numberOfNights => throw _privateConstructorUsedError;
  String? get room => throw _privateConstructorUsedError;
  String? get nutrition => throw _privateConstructorUsedError;
  @JsonKey(name: 'tour_price')
  int? get tourPrice => throw _privateConstructorUsedError;
  @JsonKey(name: 'service_charge')
  int? get serviceCharge => throw _privateConstructorUsedError;
  @JsonKey(name: 'fuel_charge')
  int? get fuelCharge => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ReservationInfoMCopyWith<ReservationInfoM> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ReservationInfoMCopyWith<$Res> {
  factory $ReservationInfoMCopyWith(
          ReservationInfoM value, $Res Function(ReservationInfoM) then) =
      _$ReservationInfoMCopyWithImpl<$Res, ReservationInfoM>;
  @useResult
  $Res call(
      {int? id,
      @JsonKey(name: 'hotel_name') String? hotelName,
      @JsonKey(name: 'hotel_adress') String? hotelAdress,
      int? horating,
      @JsonKey(name: 'rating_name') String? ratingName,
      String? departure,
      @JsonKey(name: 'arrival_country') String? arrivalCountry,
      @JsonKey(name: 'tour_date_start') String? tourDateStart,
      @JsonKey(name: 'tour_date_stop') String? tourDateStop,
      @JsonKey(name: 'number_of_nights') int? numberOfNights,
      String? room,
      String? nutrition,
      @JsonKey(name: 'tour_price') int? tourPrice,
      @JsonKey(name: 'service_charge') int? serviceCharge,
      @JsonKey(name: 'fuel_charge') int? fuelCharge});
}

/// @nodoc
class _$ReservationInfoMCopyWithImpl<$Res, $Val extends ReservationInfoM>
    implements $ReservationInfoMCopyWith<$Res> {
  _$ReservationInfoMCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? hotelName = freezed,
    Object? hotelAdress = freezed,
    Object? horating = freezed,
    Object? ratingName = freezed,
    Object? departure = freezed,
    Object? arrivalCountry = freezed,
    Object? tourDateStart = freezed,
    Object? tourDateStop = freezed,
    Object? numberOfNights = freezed,
    Object? room = freezed,
    Object? nutrition = freezed,
    Object? tourPrice = freezed,
    Object? serviceCharge = freezed,
    Object? fuelCharge = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      hotelName: freezed == hotelName
          ? _value.hotelName
          : hotelName // ignore: cast_nullable_to_non_nullable
              as String?,
      hotelAdress: freezed == hotelAdress
          ? _value.hotelAdress
          : hotelAdress // ignore: cast_nullable_to_non_nullable
              as String?,
      horating: freezed == horating
          ? _value.horating
          : horating // ignore: cast_nullable_to_non_nullable
              as int?,
      ratingName: freezed == ratingName
          ? _value.ratingName
          : ratingName // ignore: cast_nullable_to_non_nullable
              as String?,
      departure: freezed == departure
          ? _value.departure
          : departure // ignore: cast_nullable_to_non_nullable
              as String?,
      arrivalCountry: freezed == arrivalCountry
          ? _value.arrivalCountry
          : arrivalCountry // ignore: cast_nullable_to_non_nullable
              as String?,
      tourDateStart: freezed == tourDateStart
          ? _value.tourDateStart
          : tourDateStart // ignore: cast_nullable_to_non_nullable
              as String?,
      tourDateStop: freezed == tourDateStop
          ? _value.tourDateStop
          : tourDateStop // ignore: cast_nullable_to_non_nullable
              as String?,
      numberOfNights: freezed == numberOfNights
          ? _value.numberOfNights
          : numberOfNights // ignore: cast_nullable_to_non_nullable
              as int?,
      room: freezed == room
          ? _value.room
          : room // ignore: cast_nullable_to_non_nullable
              as String?,
      nutrition: freezed == nutrition
          ? _value.nutrition
          : nutrition // ignore: cast_nullable_to_non_nullable
              as String?,
      tourPrice: freezed == tourPrice
          ? _value.tourPrice
          : tourPrice // ignore: cast_nullable_to_non_nullable
              as int?,
      serviceCharge: freezed == serviceCharge
          ? _value.serviceCharge
          : serviceCharge // ignore: cast_nullable_to_non_nullable
              as int?,
      fuelCharge: freezed == fuelCharge
          ? _value.fuelCharge
          : fuelCharge // ignore: cast_nullable_to_non_nullable
              as int?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ReservationInfoMImplCopyWith<$Res>
    implements $ReservationInfoMCopyWith<$Res> {
  factory _$$ReservationInfoMImplCopyWith(_$ReservationInfoMImpl value,
          $Res Function(_$ReservationInfoMImpl) then) =
      __$$ReservationInfoMImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int? id,
      @JsonKey(name: 'hotel_name') String? hotelName,
      @JsonKey(name: 'hotel_adress') String? hotelAdress,
      int? horating,
      @JsonKey(name: 'rating_name') String? ratingName,
      String? departure,
      @JsonKey(name: 'arrival_country') String? arrivalCountry,
      @JsonKey(name: 'tour_date_start') String? tourDateStart,
      @JsonKey(name: 'tour_date_stop') String? tourDateStop,
      @JsonKey(name: 'number_of_nights') int? numberOfNights,
      String? room,
      String? nutrition,
      @JsonKey(name: 'tour_price') int? tourPrice,
      @JsonKey(name: 'service_charge') int? serviceCharge,
      @JsonKey(name: 'fuel_charge') int? fuelCharge});
}

/// @nodoc
class __$$ReservationInfoMImplCopyWithImpl<$Res>
    extends _$ReservationInfoMCopyWithImpl<$Res, _$ReservationInfoMImpl>
    implements _$$ReservationInfoMImplCopyWith<$Res> {
  __$$ReservationInfoMImplCopyWithImpl(_$ReservationInfoMImpl _value,
      $Res Function(_$ReservationInfoMImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? hotelName = freezed,
    Object? hotelAdress = freezed,
    Object? horating = freezed,
    Object? ratingName = freezed,
    Object? departure = freezed,
    Object? arrivalCountry = freezed,
    Object? tourDateStart = freezed,
    Object? tourDateStop = freezed,
    Object? numberOfNights = freezed,
    Object? room = freezed,
    Object? nutrition = freezed,
    Object? tourPrice = freezed,
    Object? serviceCharge = freezed,
    Object? fuelCharge = freezed,
  }) {
    return _then(_$ReservationInfoMImpl(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      hotelName: freezed == hotelName
          ? _value.hotelName
          : hotelName // ignore: cast_nullable_to_non_nullable
              as String?,
      hotelAdress: freezed == hotelAdress
          ? _value.hotelAdress
          : hotelAdress // ignore: cast_nullable_to_non_nullable
              as String?,
      horating: freezed == horating
          ? _value.horating
          : horating // ignore: cast_nullable_to_non_nullable
              as int?,
      ratingName: freezed == ratingName
          ? _value.ratingName
          : ratingName // ignore: cast_nullable_to_non_nullable
              as String?,
      departure: freezed == departure
          ? _value.departure
          : departure // ignore: cast_nullable_to_non_nullable
              as String?,
      arrivalCountry: freezed == arrivalCountry
          ? _value.arrivalCountry
          : arrivalCountry // ignore: cast_nullable_to_non_nullable
              as String?,
      tourDateStart: freezed == tourDateStart
          ? _value.tourDateStart
          : tourDateStart // ignore: cast_nullable_to_non_nullable
              as String?,
      tourDateStop: freezed == tourDateStop
          ? _value.tourDateStop
          : tourDateStop // ignore: cast_nullable_to_non_nullable
              as String?,
      numberOfNights: freezed == numberOfNights
          ? _value.numberOfNights
          : numberOfNights // ignore: cast_nullable_to_non_nullable
              as int?,
      room: freezed == room
          ? _value.room
          : room // ignore: cast_nullable_to_non_nullable
              as String?,
      nutrition: freezed == nutrition
          ? _value.nutrition
          : nutrition // ignore: cast_nullable_to_non_nullable
              as String?,
      tourPrice: freezed == tourPrice
          ? _value.tourPrice
          : tourPrice // ignore: cast_nullable_to_non_nullable
              as int?,
      serviceCharge: freezed == serviceCharge
          ? _value.serviceCharge
          : serviceCharge // ignore: cast_nullable_to_non_nullable
              as int?,
      fuelCharge: freezed == fuelCharge
          ? _value.fuelCharge
          : fuelCharge // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$ReservationInfoMImpl extends _ReservationInfoM {
  const _$ReservationInfoMImpl(
      {this.id,
      @JsonKey(name: 'hotel_name') this.hotelName,
      @JsonKey(name: 'hotel_adress') this.hotelAdress,
      this.horating,
      @JsonKey(name: 'rating_name') this.ratingName,
      this.departure,
      @JsonKey(name: 'arrival_country') this.arrivalCountry,
      @JsonKey(name: 'tour_date_start') this.tourDateStart,
      @JsonKey(name: 'tour_date_stop') this.tourDateStop,
      @JsonKey(name: 'number_of_nights') this.numberOfNights,
      this.room,
      this.nutrition,
      @JsonKey(name: 'tour_price') this.tourPrice,
      @JsonKey(name: 'service_charge') this.serviceCharge,
      @JsonKey(name: 'fuel_charge') this.fuelCharge})
      : super._();

  factory _$ReservationInfoMImpl.fromJson(Map<String, dynamic> json) =>
      _$$ReservationInfoMImplFromJson(json);

  @override
  final int? id;
  @override
  @JsonKey(name: 'hotel_name')
  final String? hotelName;
  @override
  @JsonKey(name: 'hotel_adress')
  final String? hotelAdress;
  @override
  final int? horating;
  @override
  @JsonKey(name: 'rating_name')
  final String? ratingName;
  @override
  final String? departure;
  @override
  @JsonKey(name: 'arrival_country')
  final String? arrivalCountry;
  @override
  @JsonKey(name: 'tour_date_start')
  final String? tourDateStart;
  @override
  @JsonKey(name: 'tour_date_stop')
  final String? tourDateStop;
  @override
  @JsonKey(name: 'number_of_nights')
  final int? numberOfNights;
  @override
  final String? room;
  @override
  final String? nutrition;
  @override
  @JsonKey(name: 'tour_price')
  final int? tourPrice;
  @override
  @JsonKey(name: 'service_charge')
  final int? serviceCharge;
  @override
  @JsonKey(name: 'fuel_charge')
  final int? fuelCharge;

  @override
  String toString() {
    return 'ReservationInfoM(id: $id, hotelName: $hotelName, hotelAdress: $hotelAdress, horating: $horating, ratingName: $ratingName, departure: $departure, arrivalCountry: $arrivalCountry, tourDateStart: $tourDateStart, tourDateStop: $tourDateStop, numberOfNights: $numberOfNights, room: $room, nutrition: $nutrition, tourPrice: $tourPrice, serviceCharge: $serviceCharge, fuelCharge: $fuelCharge)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReservationInfoMImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.hotelName, hotelName) ||
                other.hotelName == hotelName) &&
            (identical(other.hotelAdress, hotelAdress) ||
                other.hotelAdress == hotelAdress) &&
            (identical(other.horating, horating) ||
                other.horating == horating) &&
            (identical(other.ratingName, ratingName) ||
                other.ratingName == ratingName) &&
            (identical(other.departure, departure) ||
                other.departure == departure) &&
            (identical(other.arrivalCountry, arrivalCountry) ||
                other.arrivalCountry == arrivalCountry) &&
            (identical(other.tourDateStart, tourDateStart) ||
                other.tourDateStart == tourDateStart) &&
            (identical(other.tourDateStop, tourDateStop) ||
                other.tourDateStop == tourDateStop) &&
            (identical(other.numberOfNights, numberOfNights) ||
                other.numberOfNights == numberOfNights) &&
            (identical(other.room, room) || other.room == room) &&
            (identical(other.nutrition, nutrition) ||
                other.nutrition == nutrition) &&
            (identical(other.tourPrice, tourPrice) ||
                other.tourPrice == tourPrice) &&
            (identical(other.serviceCharge, serviceCharge) ||
                other.serviceCharge == serviceCharge) &&
            (identical(other.fuelCharge, fuelCharge) ||
                other.fuelCharge == fuelCharge));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      hotelName,
      hotelAdress,
      horating,
      ratingName,
      departure,
      arrivalCountry,
      tourDateStart,
      tourDateStop,
      numberOfNights,
      room,
      nutrition,
      tourPrice,
      serviceCharge,
      fuelCharge);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ReservationInfoMImplCopyWith<_$ReservationInfoMImpl> get copyWith =>
      __$$ReservationInfoMImplCopyWithImpl<_$ReservationInfoMImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$ReservationInfoMImplToJson(
      this,
    );
  }
}

abstract class _ReservationInfoM extends ReservationInfoM {
  const factory _ReservationInfoM(
          {final int? id,
          @JsonKey(name: 'hotel_name') final String? hotelName,
          @JsonKey(name: 'hotel_adress') final String? hotelAdress,
          final int? horating,
          @JsonKey(name: 'rating_name') final String? ratingName,
          final String? departure,
          @JsonKey(name: 'arrival_country') final String? arrivalCountry,
          @JsonKey(name: 'tour_date_start') final String? tourDateStart,
          @JsonKey(name: 'tour_date_stop') final String? tourDateStop,
          @JsonKey(name: 'number_of_nights') final int? numberOfNights,
          final String? room,
          final String? nutrition,
          @JsonKey(name: 'tour_price') final int? tourPrice,
          @JsonKey(name: 'service_charge') final int? serviceCharge,
          @JsonKey(name: 'fuel_charge') final int? fuelCharge}) =
      _$ReservationInfoMImpl;
  const _ReservationInfoM._() : super._();

  factory _ReservationInfoM.fromJson(Map<String, dynamic> json) =
      _$ReservationInfoMImpl.fromJson;

  @override
  int? get id;
  @override
  @JsonKey(name: 'hotel_name')
  String? get hotelName;
  @override
  @JsonKey(name: 'hotel_adress')
  String? get hotelAdress;
  @override
  int? get horating;
  @override
  @JsonKey(name: 'rating_name')
  String? get ratingName;
  @override
  String? get departure;
  @override
  @JsonKey(name: 'arrival_country')
  String? get arrivalCountry;
  @override
  @JsonKey(name: 'tour_date_start')
  String? get tourDateStart;
  @override
  @JsonKey(name: 'tour_date_stop')
  String? get tourDateStop;
  @override
  @JsonKey(name: 'number_of_nights')
  int? get numberOfNights;
  @override
  String? get room;
  @override
  String? get nutrition;
  @override
  @JsonKey(name: 'tour_price')
  int? get tourPrice;
  @override
  @JsonKey(name: 'service_charge')
  int? get serviceCharge;
  @override
  @JsonKey(name: 'fuel_charge')
  int? get fuelCharge;
  @override
  @JsonKey(ignore: true)
  _$$ReservationInfoMImplCopyWith<_$ReservationInfoMImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
