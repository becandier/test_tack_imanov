// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';

part 'about_the_hotel.g.dart';
part 'about_the_hotel.freezed.dart';

@freezed
class AboutHotelM with _$AboutHotelM {
  const factory AboutHotelM({
    String? description,
    List<String?>? peculiarities,
  }) = _AboutHotelM;
  const AboutHotelM._();

  factory AboutHotelM.fromJson(Map<String, dynamic> json) =>
      _$AboutHotelMFromJson(json);
}
