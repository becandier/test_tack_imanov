// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'about_the_hotel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$AboutHotelMImpl _$$AboutHotelMImplFromJson(Map<String, dynamic> json) =>
    _$AboutHotelMImpl(
      description: json['description'] as String?,
      peculiarities: (json['peculiarities'] as List<dynamic>?)
          ?.map((e) => e as String?)
          .toList(),
    );

Map<String, dynamic> _$$AboutHotelMImplToJson(_$AboutHotelMImpl instance) =>
    <String, dynamic>{
      'description': instance.description,
      'peculiarities': instance.peculiarities,
    };
