// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:intl/intl.dart';

part 'reservation_info.g.dart';
part 'reservation_info.freezed.dart';

@freezed
class ReservationInfoM with _$ReservationInfoM {
  const factory ReservationInfoM({
    int? id,
    @JsonKey(name: 'hotel_name') String? hotelName,
    @JsonKey(name: 'hotel_adress') String? hotelAdress,
    int? horating,
    @JsonKey(name: 'rating_name') String? ratingName,
    String? departure,
    @JsonKey(name: 'arrival_country') String? arrivalCountry,
    @JsonKey(name: 'tour_date_start') String? tourDateStart,
    @JsonKey(name: 'tour_date_stop') String? tourDateStop,
    @JsonKey(name: 'number_of_nights') int? numberOfNights,
    String? room,
    String? nutrition,
    @JsonKey(name: 'tour_price') int? tourPrice,
    @JsonKey(name: 'service_charge') int? serviceCharge,
    @JsonKey(name: 'fuel_charge') int? fuelCharge,
  }) = _ReservationInfoM;
  const ReservationInfoM._();

  factory ReservationInfoM.fromJson(Map<String, dynamic> json) =>
      _$ReservationInfoMFromJson(json);

  DateTime get tourDateStartDateTime =>
      DateFormat('dd.MM.yyyy').parse(tourDateStart ?? '');
  DateTime get tourDateStopDateTime =>
      DateFormat('dd.MM.yyyy').parse(tourDateStop ?? '');
}
