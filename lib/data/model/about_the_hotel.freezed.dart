// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'about_the_hotel.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AboutHotelM _$AboutHotelMFromJson(Map<String, dynamic> json) {
  return _AboutHotelM.fromJson(json);
}

/// @nodoc
mixin _$AboutHotelM {
  String? get description => throw _privateConstructorUsedError;
  List<String?>? get peculiarities => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AboutHotelMCopyWith<AboutHotelM> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AboutHotelMCopyWith<$Res> {
  factory $AboutHotelMCopyWith(
          AboutHotelM value, $Res Function(AboutHotelM) then) =
      _$AboutHotelMCopyWithImpl<$Res, AboutHotelM>;
  @useResult
  $Res call({String? description, List<String?>? peculiarities});
}

/// @nodoc
class _$AboutHotelMCopyWithImpl<$Res, $Val extends AboutHotelM>
    implements $AboutHotelMCopyWith<$Res> {
  _$AboutHotelMCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? description = freezed,
    Object? peculiarities = freezed,
  }) {
    return _then(_value.copyWith(
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      peculiarities: freezed == peculiarities
          ? _value.peculiarities
          : peculiarities // ignore: cast_nullable_to_non_nullable
              as List<String?>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$AboutHotelMImplCopyWith<$Res>
    implements $AboutHotelMCopyWith<$Res> {
  factory _$$AboutHotelMImplCopyWith(
          _$AboutHotelMImpl value, $Res Function(_$AboutHotelMImpl) then) =
      __$$AboutHotelMImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? description, List<String?>? peculiarities});
}

/// @nodoc
class __$$AboutHotelMImplCopyWithImpl<$Res>
    extends _$AboutHotelMCopyWithImpl<$Res, _$AboutHotelMImpl>
    implements _$$AboutHotelMImplCopyWith<$Res> {
  __$$AboutHotelMImplCopyWithImpl(
      _$AboutHotelMImpl _value, $Res Function(_$AboutHotelMImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? description = freezed,
    Object? peculiarities = freezed,
  }) {
    return _then(_$AboutHotelMImpl(
      description: freezed == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      peculiarities: freezed == peculiarities
          ? _value._peculiarities
          : peculiarities // ignore: cast_nullable_to_non_nullable
              as List<String?>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$AboutHotelMImpl extends _AboutHotelM {
  const _$AboutHotelMImpl(
      {this.description, final List<String?>? peculiarities})
      : _peculiarities = peculiarities,
        super._();

  factory _$AboutHotelMImpl.fromJson(Map<String, dynamic> json) =>
      _$$AboutHotelMImplFromJson(json);

  @override
  final String? description;
  final List<String?>? _peculiarities;
  @override
  List<String?>? get peculiarities {
    final value = _peculiarities;
    if (value == null) return null;
    if (_peculiarities is EqualUnmodifiableListView) return _peculiarities;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'AboutHotelM(description: $description, peculiarities: $peculiarities)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AboutHotelMImpl &&
            (identical(other.description, description) ||
                other.description == description) &&
            const DeepCollectionEquality()
                .equals(other._peculiarities, _peculiarities));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, description,
      const DeepCollectionEquality().hash(_peculiarities));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AboutHotelMImplCopyWith<_$AboutHotelMImpl> get copyWith =>
      __$$AboutHotelMImplCopyWithImpl<_$AboutHotelMImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$AboutHotelMImplToJson(
      this,
    );
  }
}

abstract class _AboutHotelM extends AboutHotelM {
  const factory _AboutHotelM(
      {final String? description,
      final List<String?>? peculiarities}) = _$AboutHotelMImpl;
  const _AboutHotelM._() : super._();

  factory _AboutHotelM.fromJson(Map<String, dynamic> json) =
      _$AboutHotelMImpl.fromJson;

  @override
  String? get description;
  @override
  List<String?>? get peculiarities;
  @override
  @JsonKey(ignore: true)
  _$$AboutHotelMImplCopyWith<_$AboutHotelMImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
