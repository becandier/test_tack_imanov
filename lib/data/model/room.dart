// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:test_task_imanov/data/_data.dart';

part 'room.g.dart';
part 'room.freezed.dart';

@freezed
class RoomM with _$RoomM {
  const factory RoomM({
    int? id,
    String? name,
    int? price,
    @JsonKey(name: 'price_per') String? pricePer,
    List<String?>? peculiarities,
    @JsonKey(name: 'image_urls') List<String?>? imageUrls,
  }) = _RoomM;
  const RoomM._();

  factory RoomM.fromJson(Map<String, dynamic> json) => _$RoomMFromJson(json);
}
