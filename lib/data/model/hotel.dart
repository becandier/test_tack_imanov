// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:test_task_imanov/data/_data.dart';

part 'hotel.g.dart';
part 'hotel.freezed.dart';

@freezed
class HotelM with _$HotelM {
  const factory HotelM({
    int? id,
    String? name,
    String? adress,
    @JsonKey(name: 'minimal_price') int? minimalPrice,
    @JsonKey(name: 'price_for_it') String? priceForIt,
    int? rating,
    @JsonKey(name: 'rating_name') String? ratingName,
    @JsonKey(name: 'image_urls') List<String?>? imageUrls,
    @JsonKey(name: 'about_the_hotel') AboutHotelM? aboutHotel,
  }) = _HotelM;
  const HotelM._();

  factory HotelM.fromJson(Map<String, dynamic> json) => _$HotelMFromJson(json);
}
