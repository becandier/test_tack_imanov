import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:test_task_imanov/data/_data.dart';

part 'main_api.g.dart';

@RestApi(baseUrl: 'https://run.mocky.io/v3/')
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  ///Get hotel
  @GET('/d144777c-a67f-4e35-867a-cacc3b827473')
  Future<HotelM?> getHotel();

  ///Get list of rooms
  @GET('/8b532701-709e-4194-a41c-1a903af00195')
  Future<RoomResponseM?> getRooms();

  ///Get info about reservation
  @GET('/63866c74-d593-432c-af8e-f279d1a8d2ff')
  Future<ReservationInfoM?> getReservationInfo();
}
