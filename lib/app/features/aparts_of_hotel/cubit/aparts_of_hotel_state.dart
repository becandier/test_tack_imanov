part of 'aparts_of_hotel_cubit.dart';

class ApartsOfHotelState extends Equatable {
  const ApartsOfHotelState({
    this.rooms,
    this.isLoading = false,
  });

  final List<RoomM?>? rooms;

  final bool? isLoading;

  @override
  List<Object?> get props => [
        rooms,
        isLoading,
      ];

  ApartsOfHotelState copyWith({
    List<RoomM?>? rooms,
    bool? isLoading,
  }) {
    return ApartsOfHotelState(
      rooms: rooms ?? this.rooms,
      isLoading: isLoading ?? this.isLoading,
    );
  }
}
