import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:test_task_imanov/data/_data.dart';

part 'aparts_of_hotel_state.dart';

class ApartsOfHotelCubit extends Cubit<ApartsOfHotelState> {
  ApartsOfHotelCubit({required this.mainRepository})
      : super(const ApartsOfHotelState()) {
    init();
  }

  final MainRepository mainRepository;

  Future<void> init() async {
    await Future.wait([
      fetchInitData(),
    ]);
  }

  Future<void> refresh() async {
    await Future.wait([
      fetchInitData(),
    ]);
  }

  Future<void> fetchInitData() async {
    emit(state.copyWith(isLoading: true));
    await Future.wait([
      getRooms(),
    ]);
    emit(state.copyWith(isLoading: false));
  }

  Future<void> getRooms() async {
    final roomsResponse = await mainRepository.getRooms();
    emit(state.copyWith(rooms: roomsResponse?.rooms));
  }
}
