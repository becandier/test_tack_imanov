import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_task_imanov/app/_app.dart';

class ApartsOfHotelBody extends StatelessWidget {
  const ApartsOfHotelBody({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ApartsOfHotelCubit, ApartsOfHotelState>(
      builder: (context, state) {
        if (state.isLoading ?? false) {
          return const Center(child: CircularProgressIndicator.adaptive());
        }
        return RefreshIndicator.adaptive(
          onRefresh: () async {
            unawaited(context.read<ApartsOfHotelCubit>().refresh());
          },
          child: ListView.separated(
            itemCount: state.rooms?.length ?? 0,
            separatorBuilder: (context, index) => const SizedBox(height: 8),
            itemBuilder: (context, index) {
              final room = state.rooms?[index];
              return RoomItemWidget(room: room);
            },
          ),
        );
      },
    );
  }
}
