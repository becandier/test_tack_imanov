import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_task_imanov/app/_app.dart';
import 'package:test_task_imanov/data/_data.dart';

@RoutePage()
class ApartsOfHotelPage extends StatelessWidget {
  const ApartsOfHotelPage({required this.hotelName, super.key});

  final String? hotelName;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ApartsOfHotelCubit(
        mainRepository: context.read<MainRepository>(),
      ),
      child: ApartsOfHotelView(
        hotelName: hotelName,
      ),
    );
  }
}

class ApartsOfHotelView extends StatelessWidget {
  const ApartsOfHotelView({
    required this.hotelName,
    super.key,
  });

  final String? hotelName;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(hotelName ?? '')),
      body: const ApartsOfHotelBody(),
    );
  }
}
