import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:test_task_imanov/app/_app.dart';
import 'package:test_task_imanov/data/_data.dart';

///Card of room item widget
class RoomItemWidget extends StatelessWidget {
  ///Card of room item widget

  const RoomItemWidget({
    required this.room,
    super.key,
  });

  final RoomM? room;

  @override
  Widget build(BuildContext context) {
    return DefaultContainerWidget(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //Images slider
          ImageSliderWidget(
            imagesPath: room?.imageUrls,
          ),
          const SizedBox(height: 8),
          // Name room
          Text(
            room?.name ?? '',
            style: Theme.of(context).textTheme.titleLarge,
          ),
          const SizedBox(height: 8),
          PeculiaritiesWrapWidget(
            peculiarities: room?.peculiarities ?? [],
          ),
          const SizedBox(height: 8),
          NavigationTextButtonWidget(
            onPressed: () {},
            text: 'Подробнее о номере',
          ),
          const SizedBox(height: 16),
          //Info about price
          PriceRowWidget(
            priceText: '${NumbersFormat.numberSpace(room?.price ?? 0)} ₽',
            priceTitle: (room?.pricePer ?? '').toLowerCase(),
          ),
          const SizedBox(height: 16),
          ElevatedButton(
            onPressed: () {
              context.router.push(const ReservationRoute());
            },
            child: const Text('Выбрать номер'),
          ),
        ],
      ),
    );
  }
}
