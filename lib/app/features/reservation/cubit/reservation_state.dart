part of 'reservation_cubit.dart';

class ReservationState extends Equatable {
  const ReservationState({
    this.reservationInfo,
    this.totalPrice,
    this.isLoading = false,
  });

  final ReservationInfoM? reservationInfo;

  final int? totalPrice;

  final bool? isLoading;

  @override
  List<Object?> get props => [
        reservationInfo,
        totalPrice,
        isLoading,
      ];

  ReservationState copyWith({
    ReservationInfoM? reservationInfo,
    int? totalPrice,
    bool? isLoading,
  }) {
    return ReservationState(
      reservationInfo: reservationInfo ?? this.reservationInfo,
      totalPrice: totalPrice ?? this.totalPrice,
      isLoading: isLoading ?? this.isLoading,
    );
  }
}
