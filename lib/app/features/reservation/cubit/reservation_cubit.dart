import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:test_task_imanov/data/_data.dart';

part 'reservation_state.dart';

class ReservationCubit extends Cubit<ReservationState> {
  ReservationCubit({
    required this.mainRepository,
  }) : super(const ReservationState()) {
    init();
  }

  final MainRepository mainRepository;

  Future<void> init() async {
    await Future.wait([
      fetchInitData(),
    ]);
  }

  Future<void> refresh() async {
    await Future.wait([
      fetchInitData(),
    ]);
  }

  Future<void> fetchInitData() async {
    emit(state.copyWith(isLoading: true));
    await Future.wait([
      getReservationInfo(),
    ]);
    emit(state.copyWith(isLoading: false));
  }

  Future<void> getReservationInfo() async {
    final reservationInfo = await mainRepository.getReservationInfo();
    final totalPrice = (reservationInfo?.tourPrice ?? 0) +
        (reservationInfo?.serviceCharge ?? 0) +
        (reservationInfo?.fuelCharge ?? 0);
    emit(
      state.copyWith(
        reservationInfo: reservationInfo,
        totalPrice: totalPrice,
      ),
    );
  }
}
