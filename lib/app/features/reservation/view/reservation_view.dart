import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_task_imanov/app/_app.dart';
import 'package:test_task_imanov/app/theme/dialog_modals/dialog_modals.dart';
import 'package:test_task_imanov/data/_data.dart';

@RoutePage()
class ReservationPage extends StatelessWidget {
  const ReservationPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ReservationCubit(
        mainRepository: context.read<MainRepository>(),
      ),
      child: const ReservationView(),
    );
  }
}

class ReservationView extends StatefulWidget {
  const ReservationView({super.key});

  @override
  State<ReservationView> createState() => _ReservationViewState();
}

class _ReservationViewState extends State<ReservationView> {
  late GlobalKey<FormState> _formKey;
  @override
  void initState() {
    _formKey = GlobalKey<FormState>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Бронирование')),
      body: ReservationBody(
        formKey: _formKey,
      ),
      bottomNavigationBar: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
          child: ElevatedButton(
            onPressed: () {
              if (_formKey.currentState!.validate()) {
                context.router.push(const SuccessRoute());
              } else {
                DialogModals.showErrorSnackBar(
                  context,
                  textError: 'Некоторые поля незаполнены',
                );
              }
            },
            child: BlocBuilder<ReservationCubit, ReservationState>(
              builder: (context, state) {
                if (state.isLoading ?? false) {
                  return const CircularProgressIndicator();
                }
                return Text(
                    'Оплатить ${NumbersFormat.numberSpace(state.totalPrice ?? 0)} ₽');
              },
            ),
          ),
        ),
      ),
    );
  }
}
