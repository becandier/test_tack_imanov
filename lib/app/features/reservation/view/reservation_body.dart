import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_task_imanov/app/_app.dart';

class ReservationBody extends StatefulWidget {
  const ReservationBody({required this.formKey, super.key});

  final GlobalKey<FormState> formKey;

  @override
  State<ReservationBody> createState() => _ReservationBodyState();
}

class _ReservationBodyState extends State<ReservationBody> {
  late int countTourists;
  @override
  void initState() {
    countTourists = 2;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ReservationCubit, ReservationState>(
      builder: (context, state) {
        if (state.isLoading ?? false) {
          return const Center(child: CircularProgressIndicator.adaptive());
        }
        final reservationInfo = state.reservationInfo;
        return Form(
          key: widget.formKey,
          child: CustomScrollView(
            slivers: [
              SliverList.list(
                children: [
                  DefaultContainerWidget(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        //Widget rating of hotel
                        RatingHotelWidget(
                          ratingName: reservationInfo?.ratingName,
                          ratingNumber: reservationInfo?.horating,
                        ),
                        const SizedBox(height: 8),
                        //Name hotel
                        Text(
                          reservationInfo?.hotelName ?? '',
                          style: Theme.of(context).textTheme.titleLarge,
                        ),
                        const SizedBox(height: 8),
                        // Address hotel (button)
                        TextButton(
                          onPressed: () {},
                          child: Text(reservationInfo?.hotelAdress ?? ''),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 8),
                  TableRoomInfoWidget(reservationInfo: reservationInfo),
                  const SizedBox(height: 8),
                  const InfoAboutClientSectionWidget(),
                  const SizedBox(height: 8),
                ],
              ),
              TouristsDataSectionWidget(
                countTourist: countTourists,
              ),
              AddTouristButton(
                onAdd: () {
                  setState(() {
                    countTourists += 1;
                  });
                },
              ),
              SliverList.list(
                children: [
                  const SizedBox(height: 8),
                  TotalPriceReservationWidget(
                    tourPrice: reservationInfo?.tourPrice,
                    serviceCharge: reservationInfo?.serviceCharge,
                    fuelCharge: reservationInfo?.fuelCharge,
                    totalPrice: state.totalPrice,
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
