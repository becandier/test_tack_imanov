import 'package:flutter/material.dart';

import 'package:test_task_imanov/app/_app.dart';

class InfoAboutClientSectionWidget extends StatelessWidget {
  const InfoAboutClientSectionWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return DefaultContainerWidget(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Информация о покупателе',
            style: Theme.of(context).textTheme.titleLarge,
          ),
          const SizedBox(height: 20),
          Column(
            children: [
              TextFormField(
                inputFormatters: [
                  NumbersFormat.kPhoneMaskFormatterWithPrefix,
                ],
                keyboardType: TextInputType.phone,
                decoration: const InputDecoration(
                  labelText: 'Номер телефона',
                  hintText: '+7 (***) ***-**-**',
                ),
              ),
              const SizedBox(height: 8),
              const EmailTextFormFiled(),
              const SizedBox(height: 8),
              Text(
                'Эти данные никому не передаются. После оплаты мы вышли чек на указанный вами номер и почту',
                style: Theme.of(context).textTheme.labelSmall!.copyWith(
                      fontSize: 14,
                    ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
