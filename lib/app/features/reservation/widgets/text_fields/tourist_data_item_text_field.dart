import 'package:flutter/material.dart';
import 'package:test_task_imanov/app/_app.dart';

class TouristDataItemTextField extends StatefulWidget {
  const TouristDataItemTextField({
    required this.hintText,
    super.key,
  });

  final String hintText;

  @override
  State<TouristDataItemTextField> createState() =>
      _TouristDataItemTextFieldState();
}

class _TouristDataItemTextFieldState extends State<TouristDataItemTextField> {
  String? errorText;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
        labelText: widget.hintText,
        fillColor:
            errorText == null ? null : context.appColors.red!.withOpacity(0.1),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          setState(() {
            errorText = 'Поле не может быть пустым';
          });
          return 'Поле не может быть пустым';
        }
        return null;
      },
    );
  }
}
