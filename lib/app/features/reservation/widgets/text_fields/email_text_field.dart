import 'package:flutter/material.dart';
import 'package:test_task_imanov/app/_app.dart';

///Email text form field with Validation
class EmailTextFormFiled extends StatefulWidget {
  ///Email text form field with Validation

  const EmailTextFormFiled({super.key});

  @override
  _EmailTextFormFiledState createState() => _EmailTextFormFiledState();
}

class _EmailTextFormFiledState extends State<EmailTextFormFiled> {
  final _formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  String? errorText;

  String? validateEmail(String? value) {
    if ((value ?? '').isEmpty) {
      return null;
    }

    if (!ValidationCheck.isValidEmail(value ?? '')) {
      return 'Некорректный email';
    }

    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          TextFormField(
            onTapOutside: (_) {
              setState(() {
                errorText = validateEmail(emailController.text);
                _formKey.currentState!.validate();
              });
            },
            controller: emailController,
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              labelText: 'Почта',
              fillColor: errorText == null
                  ? null
                  : context.appColors.red!.withOpacity(0.1),
            ),
            validator: (value) => errorText,
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    emailController.dispose();
    super.dispose();
  }
}
