import 'package:flutter/material.dart';
import 'package:test_task_imanov/app/_app.dart';

class TotalPriceReservationWidget extends StatelessWidget {
  const TotalPriceReservationWidget({
    required this.tourPrice,
    required this.serviceCharge,
    required this.fuelCharge,
    required this.totalPrice,
    super.key,
  });

  final int? tourPrice;
  final int? fuelCharge;
  final int? serviceCharge;
  final int? totalPrice;

  @override
  Widget build(BuildContext context) {
    return DefaultContainerWidget(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Тур',
                style: Theme.of(context).textTheme.labelSmall,
              ),
              Text(
                '${NumbersFormat.numberSpace(
                  tourPrice ?? 0,
                )} ₽',
                style: Theme.of(context).textTheme.labelLarge,
              ),
            ],
          ),
          const SizedBox(height: 8),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Топливный сбор',
                style: Theme.of(context).textTheme.labelSmall,
              ),
              Text(
                '${NumbersFormat.numberSpace(
                  serviceCharge ?? 0,
                )} ₽',
                style: Theme.of(context).textTheme.labelLarge,
              ),
            ],
          ),
          const SizedBox(height: 8),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Сервисный сбор',
                style: Theme.of(context).textTheme.labelSmall,
              ),
              Text(
                '${NumbersFormat.numberSpace(
                  fuelCharge ?? 0,
                )} ₽',
                style: Theme.of(context).textTheme.labelLarge,
              ),
            ],
          ),
          const SizedBox(height: 8),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'К оплате',
                style: Theme.of(context).textTheme.labelSmall,
              ),
              Text(
                '${NumbersFormat.numberSpace(
                  totalPrice ?? 0,
                )} ₽',
                style: Theme.of(context).textTheme.labelLarge!.copyWith(
                      color: context.appColors.primary,
                      fontWeight: FontWeight.bold,
                    ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
