import 'package:flutter/material.dart';
import 'package:test_task_imanov/app/_app.dart';

class TouristsDataSectionWidget extends StatefulWidget {
  const TouristsDataSectionWidget({required this.countTourist, super.key});
  final int countTourist;

  @override
  State<TouristsDataSectionWidget> createState() =>
      _TouristsDataSectionWidgetState();
}

class _TouristsDataSectionWidgetState extends State<TouristsDataSectionWidget> {
  @override
  Widget build(BuildContext context) {
    return SliverList.separated(
      separatorBuilder: (context, index) => const SizedBox(height: 8),
      itemCount: widget.countTourist,
      itemBuilder: (context, index) => CustomExpansionTileWidget(
        isExpandedInitial: index == 0,
        maintainState: index == 0,
        title: Builder(
          builder: (context) {
            final titleText =
                '${ToWordsOrdinalConverter.convert(index + 1) ?? '${index + 1}'} турист';
            return Text(
              titleText,
              style: Theme.of(context).textTheme.titleLarge,
            );
          },
        ),
        children: const [
          SizedBox(height: 8),
          TouristDataItemTextField(
            hintText: 'Имя',
          ),
          SizedBox(height: 8),
          TouristDataItemTextField(
            hintText: 'Фамилия',
          ),
          SizedBox(height: 8),
          TouristDataItemTextField(
            hintText: 'Дата рождения',
          ),
          SizedBox(height: 8),
          TouristDataItemTextField(
            hintText: 'Гражданство',
          ),
          SizedBox(height: 8),
          TouristDataItemTextField(
            hintText: 'Номер загранпаспорта',
          ),
          SizedBox(height: 8),
          TouristDataItemTextField(
            hintText: 'Срок действия загранпаспорта',
          ),
          SizedBox(height: 16),
        ],
      ),
    );
  }
}
