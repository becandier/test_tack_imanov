import 'package:flutter/material.dart';
import 'package:test_task_imanov/app/_app.dart';

class AddTouristButton extends StatelessWidget {
  const AddTouristButton({required this.onAdd, super.key});
  final VoidCallback onAdd;

  @override
  Widget build(BuildContext context) {
    return SliverList.list(
      children: [
        const SizedBox(height: 8),
        DefaultContainerWidget(
          padding: EdgeInsets.zero,
          child: ListTile(
            onTap: onAdd,
            title: Text(
              'Добавить туриста',
              style: Theme.of(context).textTheme.titleLarge,
            ),
            trailing: DecoratedBox(
              decoration: BoxDecoration(
                color: context.appColors.primary,
                borderRadius: BorderRadius.circular(5),
              ),
              child: Padding(
                padding: const EdgeInsets.all(5),
                child: Icon(Icons.add, color: context.appColors.white),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
