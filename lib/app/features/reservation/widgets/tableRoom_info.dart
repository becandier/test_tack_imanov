import 'package:flutter/material.dart';
import 'package:test_task_imanov/app/_app.dart';
import 'package:test_task_imanov/data/_data.dart';

class TableRoomInfoWidget extends StatelessWidget {
  const TableRoomInfoWidget({
    required this.reservationInfo,
    super.key,
  });

  final ReservationInfoM? reservationInfo;

  @override
  Widget build(BuildContext context) {
    TableRow buildTableRow(String title, String textValue) {
      return TableRow(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8),
            child: Text(
              title,
              style: Theme.of(context).textTheme.labelSmall,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8),
            child: Text(
              textValue,
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ),
        ],
      );
    }

    return DefaultContainerWidget(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Table(
            columnWidths: const {
              0: FlexColumnWidth(2),
              1: FlexColumnWidth(3),
            },
            children: [
              buildTableRow(
                'Вылет из',
                reservationInfo?.departure ?? '',
              ),
              buildTableRow(
                'Страна, город',
                reservationInfo?.arrivalCountry ?? '',
              ),
              buildTableRow(
                'Даты',
                '${reservationInfo?.tourDateStart} - ${reservationInfo?.tourDateStop}',
              ),
              buildTableRow(
                'Кол-во ночей',
                (reservationInfo?.numberOfNights ?? 0).toString(),
              ),
              buildTableRow(
                'Отель',
                reservationInfo?.hotelName ?? '',
              ),
              buildTableRow(
                'Номер',
                reservationInfo?.room ?? '',
              ),
              buildTableRow(
                'Питание',
                reservationInfo?.nutrition ?? '',
              ),
            ],
          ),
        ],
      ),
    );
  }
}
