import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:test_task_imanov/data/_data.dart';

part 'hotel_item_state.dart';

class HotelItemCubit extends Cubit<HotelItemState> {
  HotelItemCubit({required this.mainRepository})
      : super(const HotelItemState()) {
    init();
  }
  final MainRepository mainRepository;

  Future<void> init() async {
    await Future.wait([
      fetchInitData(),
    ]);
  }

  Future<void> refresh() async {
    await Future.wait([
      fetchInitData(),
    ]);
  }

  Future<void> fetchInitData() async {
    emit(state.copyWith(isLoading: true));
    await Future.wait([
      getHotels(),
    ]);
    emit(state.copyWith(isLoading: false));
  }

  Future<void> getHotels() async {
    final hotel = await mainRepository.getHotel();
    emit(state.copyWith(hotel: hotel));
  }
}
