part of 'hotel_item_cubit.dart';

class HotelItemState extends Equatable {
  const HotelItemState({
    this.hotel,
    this.isLoading = false,
  });

  final HotelM? hotel;

  final bool? isLoading;

  @override
  List<Object?> get props => [
        hotel,
        isLoading,
      ];

  HotelItemState copyWith({
    HotelM? hotel,
    bool? isLoading,
  }) {
    return HotelItemState(
      hotel: hotel ?? this.hotel,
      isLoading: isLoading ?? this.isLoading,
    );
  }
}
