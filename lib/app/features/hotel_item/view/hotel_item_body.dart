import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_task_imanov/app/_app.dart';

class HotelItemBody extends StatelessWidget {
  const HotelItemBody({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HotelItemCubit, HotelItemState>(
      builder: (context, state) {
        if (state.isLoading ?? false) {
          return const Center(child: CircularProgressIndicator.adaptive());
        }
        return RefreshIndicator.adaptive(
          onRefresh: () async {
            unawaited(context.read<HotelItemCubit>().refresh());
          },
          child: ListView(
            children: [
              const SizedBox(height: 10),
              //Section 1
              DefaultContainerWidget(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //Images slider
                    ImageSliderWidget(
                      imagesPath: state.hotel?.imageUrls,
                    ),
                    const SizedBox(height: 15),
                    //Widget rating of hotel
                    RatingHotelWidget(
                      ratingName: state.hotel?.ratingName,
                      ratingNumber: state.hotel?.rating,
                    ),
                    const SizedBox(height: 8),
                    //Name hotel
                    Text(
                      state.hotel?.name ?? '',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const SizedBox(height: 8),
                    // Address hotel (button)
                    TextButton(
                      onPressed: () {},
                      child: Text(state.hotel?.adress ?? ''),
                    ),
                    const SizedBox(height: 15),
                    //Info about price
                    PriceRowWidget(
                      priceText: 'от ${NumbersFormat.numberSpace(
                        state.hotel?.minimalPrice ?? 0,
                      )} ₽',
                      priceTitle: (state.hotel?.priceForIt ?? '').toLowerCase(),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 8),
              //Section 2
              DefaultContainerWidget(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Об отеле',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const SizedBox(height: 15),
                    //Peculiarities hotel
                    PeculiaritiesWrapWidget(
                      peculiarities:
                          state.hotel?.aboutHotel?.peculiarities ?? [],
                    ),
                    const SizedBox(height: 10),
                    // Description hotel
                    Text(
                      state.hotel?.aboutHotel?.description ?? '',
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                    const SizedBox(height: 15),
                    //Parametres hotel
                    const ParametresHotelWidget(),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
