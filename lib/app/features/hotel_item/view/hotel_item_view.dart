import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_task_imanov/app/_app.dart';
import 'package:test_task_imanov/data/_data.dart';

@RoutePage()
class HotelItemPage extends StatelessWidget {
  const HotelItemPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HotelItemCubit(
        mainRepository: context.read<MainRepository>(),
      ),
      child: const HotelItemView(),
    );
  }
}

class HotelItemView extends StatelessWidget {
  const HotelItemView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Отель')),
      body: const HotelItemBody(),
      bottomNavigationBar: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
          child: ElevatedButton(
            onPressed: () {
              final hotelName =
                  context.read<HotelItemCubit>().state.hotel?.name;
              context.router.push(
                ApartsOfHotelRoute(
                  hotelName: hotelName,
                ),
              );
            },
            child: const Text('К выбору номера'),
          ),
        ),
      ),
    );
  }
}
