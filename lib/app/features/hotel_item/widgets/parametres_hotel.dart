import 'package:flutter/material.dart';
import 'package:iconsax/iconsax.dart';
import 'package:test_task_imanov/app/_app.dart';

class ParametresHotelWidget extends StatelessWidget {
  const ParametresHotelWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        color: context.appColors.gray,
        borderRadius: BorderRadius.circular(15),
      ),
      child: const Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 10,
        ),
        child: Column(
          children: [
            CustomListTileWidget(
              titleText: 'Удобства',
              subtitleText: 'Самое необходимое',
              leading: Icon(Iconsax.emoji_happy, size: 28),
            ),
            Divider(),
            CustomListTileWidget(
              titleText: 'Что включено',
              subtitleText: 'Самое необходимое',
              leading: Icon(Iconsax.tick_square, size: 28),
            ),
            Divider(),
            CustomListTileWidget(
              titleText: 'Что не включено',
              subtitleText: 'Самое необходимое',
              leading: Icon(Iconsax.close_square, size: 28),
            ),
          ],
        ),
      ),
    );
  }
}
