import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:test_task_imanov/app/_app.dart';

@RoutePage()
class SuccessPage extends StatelessWidget {
  const SuccessPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const SuccessView();
  }
}

class SuccessView extends StatelessWidget {
  const SuccessView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: context.appColors.white,
      appBar: AppBar(
        title: const Text('Заказ оплачен'),
      ),
      body: const SuccessPageBody(),
      bottomNavigationBar: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
          child: ElevatedButton(
            onPressed: () {
              context.router.popUntilRoot();
            },
            child: const Text('Супер!'),
          ),
        ),
      ),
    );
  }
}
