import 'dart:math';

import 'package:flutter/material.dart';
import 'package:test_task_imanov/app/_app.dart';

class SuccessPageBody extends StatelessWidget {
  const SuccessPageBody({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 32),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ClipOval(
              child: SizedBox(
                child: DecoratedBox(
                  decoration: BoxDecoration(color: context.appColors.gray),
                  child: Padding(
                    padding: const EdgeInsets.all(25),
                    child: Image.asset(
                      AssetsPath.partyImagePng,
                      height: 44,
                      width: 44,
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 32),
            Text(
              'Ваш заказ принят в работу',
              style: Theme.of(context).textTheme.titleLarge,
            ),
            const SizedBox(height: 20),
            Text(
              'Подтверждение заказа №"${Random.secure().nextInt(10000)}" может занять некоторое время (от 1 часа до суток). Как только мы получим ответ от туроператора, вам на почту придет уведомление.',
              style: Theme.of(context).textTheme.labelMedium,
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
