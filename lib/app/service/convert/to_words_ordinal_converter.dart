class ToWordsOrdinalConverter {
  ///Convert number to ordinal word
  ///!max 20
  static String? convert(int number) {
    switch (number) {
      case 1:
        return 'Первый';
      case 2:
        return 'Второй';
      case 3:
        return 'Третий';
      case 4:
        return 'Четвертый';
      case 5:
        return 'Пятый';
      case 6:
        return 'Шестый';
      case 7:
        return 'Седьмый';
      case 8:
        return 'Восьмый';
      case 9:
        return 'Девятый';
      case 10:
        return 'Десятый';
      case 11:
        return 'Одиннадцатый';
      case 12:
        return 'Двенадцатый';
      case 13:
        return 'Тринадцатый';
      case 14:
        return 'Четырнадцатый';
      case 15:
        return 'Пятнадцатый';
      case 16:
        return 'Шестнадцатый';
      case 17:
        return 'Седьмнадцатый';
      case 18:
        return 'Восемнадцатый';
      case 19:
        return 'Девятнадцатый';
      case 20:
        return 'Двадцатый';
      default:
        return null;
    }
  }
}
