import 'package:intl/intl.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class NumbersFormat {
  ///Format with space bentween numbers
  ///1000 -> 1 000
  static String numberSpace(int number) {
    return NumberFormat.decimalPattern('uk-UA').format(number);
  }

  /// Маска для номера телефона с префиксом (c "+7")
  static final kPhoneMaskFormatterWithPrefix = MaskTextInputFormatter(
    mask: '+7 (###) ###-##-##',
    filter: {'#': RegExp('[0-9]')},
  );
}
