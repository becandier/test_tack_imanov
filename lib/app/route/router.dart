import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:test_task_imanov/app/features/_features.dart';

part 'router.gr.dart';

@AutoRouterConfig()
class AppRouter extends _$AppRouter {
  AppRouter();

  @override
  List<AutoRoute> get routes => [
        RedirectRoute(path: '/', redirectTo: '/hotel_item'),
        AutoRoute(
          path: '/hotel_item',
          page: HotelItemRoute.page,
        ),
        AutoRoute(
          path: '/aparts_of_hotel',
          page: ApartsOfHotelRoute.page,
        ),
        AutoRoute(
          path: '/reservation',
          page: ReservationRoute.page,
        ),
        AutoRoute(
          path: '/success',
          page: SuccessRoute.page,
        ),
      ];
}
