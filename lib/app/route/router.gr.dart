// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

part of 'router.dart';

abstract class _$AppRouter extends RootStackRouter {
  // ignore: unused_element
  _$AppRouter({super.navigatorKey});

  @override
  final Map<String, PageFactory> pagesMap = {
    ApartsOfHotelRoute.name: (routeData) {
      final args = routeData.argsAs<ApartsOfHotelRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: ApartsOfHotelPage(
          hotelName: args.hotelName,
          key: args.key,
        ),
      );
    },
    HotelItemRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const HotelItemPage(),
      );
    },
    ReservationRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const ReservationPage(),
      );
    },
    SuccessRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const SuccessPage(),
      );
    },
  };
}

/// generated route for
/// [ApartsOfHotelPage]
class ApartsOfHotelRoute extends PageRouteInfo<ApartsOfHotelRouteArgs> {
  ApartsOfHotelRoute({
    required String? hotelName,
    Key? key,
    List<PageRouteInfo>? children,
  }) : super(
          ApartsOfHotelRoute.name,
          args: ApartsOfHotelRouteArgs(
            hotelName: hotelName,
            key: key,
          ),
          initialChildren: children,
        );

  static const String name = 'ApartsOfHotelRoute';

  static const PageInfo<ApartsOfHotelRouteArgs> page =
      PageInfo<ApartsOfHotelRouteArgs>(name);
}

class ApartsOfHotelRouteArgs {
  const ApartsOfHotelRouteArgs({
    required this.hotelName,
    this.key,
  });

  final String? hotelName;

  final Key? key;

  @override
  String toString() {
    return 'ApartsOfHotelRouteArgs{hotelName: $hotelName, key: $key}';
  }
}

/// generated route for
/// [HotelItemPage]
class HotelItemRoute extends PageRouteInfo<void> {
  const HotelItemRoute({List<PageRouteInfo>? children})
      : super(
          HotelItemRoute.name,
          initialChildren: children,
        );

  static const String name = 'HotelItemRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [ReservationPage]
class ReservationRoute extends PageRouteInfo<void> {
  const ReservationRoute({List<PageRouteInfo>? children})
      : super(
          ReservationRoute.name,
          initialChildren: children,
        );

  static const String name = 'ReservationRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [SuccessPage]
class SuccessRoute extends PageRouteInfo<void> {
  const SuccessRoute({List<PageRouteInfo>? children})
      : super(
          SuccessRoute.name,
          initialChildren: children,
        );

  static const String name = 'SuccessRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}
