import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:test_task_imanov/app/_app.dart';

class BulletsWidget extends StatelessWidget {
  const BulletsWidget({
    required this.activeIndex,
    required this.count,
    required this.diameter,
    super.key,
  });

  final int activeIndex;
  final int count;
  final double diameter;
  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        color: context.appColors.white,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 5,
        ),
        child: AnimatedSmoothIndicator(
          count: count,
          activeIndex: activeIndex,
          effect: ScrollingDotsEffect(
            activeDotScale: 1,
            spacing: 5,
            dotColor: Theme.of(context).colorScheme.onSurface.withOpacity(0.5),
            activeDotColor: context.appColors.black!,
            dotHeight: diameter,
            dotWidth: diameter,
          ),
        ),
      ),
    );
  }
}
