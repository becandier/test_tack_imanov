import 'dart:math';

import 'package:flutter/material.dart';
import 'package:test_task_imanov/app/_app.dart';

///Look like outlined button with arrow (navigation)
class NavigationTextButtonWidget extends StatelessWidget {
  ///Look like outlined button with arrow (navigation)
  const NavigationTextButtonWidget({
    required this.text,
    required this.onPressed,
    super.key,
  });

  final String text;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: DecoratedBox(
        decoration: BoxDecoration(
          color: context.appColors.primary!.withOpacity(0.1),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 10,
            vertical: 5,
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                text,
                style: Theme.of(context)
                    .textTheme
                    .labelLarge!
                    .copyWith(color: context.appColors.primary),
              ),
              Transform.rotate(
                angle: pi,
                child: Icon(
                  Icons.arrow_back_ios,
                  size: 18,
                  color: context.appColors.primary,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
