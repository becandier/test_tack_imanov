import 'dart:math';

import 'package:flutter/material.dart';
import 'package:iconsax/iconsax.dart';
import 'package:test_task_imanov/app/_app.dart';

class CustomExpansionTileWidget extends StatefulWidget {
  const CustomExpansionTileWidget({
    required this.title,
    required this.children,
    this.isExpandedInitial = false,
    this.maintainState = false,
    super.key,
  });
  final Widget title;
  final List<Widget> children;

  final bool isExpandedInitial;
  final bool maintainState;

  @override
  State<CustomExpansionTileWidget> createState() =>
      _CustomExpansionTileWidgetState();
}

class _CustomExpansionTileWidgetState extends State<CustomExpansionTileWidget> {
  late bool isExpand;
  @override
  void initState() {
    isExpand = widget.isExpandedInitial;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: context.appColors.white,
      ),
      child: ExpansionTile(
        maintainState: widget.maintainState,
        initiallyExpanded: widget.isExpandedInitial,
        childrenPadding: const EdgeInsets.symmetric(horizontal: 10),
        onExpansionChanged: (value) => setState(() => isExpand = value),
        title: widget.title,
        trailing: DecoratedBox(
          decoration: BoxDecoration(
            color: context.appColors.primary!.withOpacity(0.1),
            borderRadius: BorderRadius.circular(5),
          ),
          child: Padding(
            padding: const EdgeInsets.all(5),
            child: AnimatedRotation(
              turns: isExpand ? pi / 6.3 : 0,
              duration: const Duration(milliseconds: 200),
              child:
                  Icon(Iconsax.arrow_down_14, color: context.appColors.primary),
            ),
          ),
        ),
        children: widget.children,
      ),
    );
  }
}
