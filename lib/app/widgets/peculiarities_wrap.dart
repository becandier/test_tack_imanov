import 'package:flutter/material.dart';
import 'package:test_task_imanov/app/_app.dart';

class PeculiaritiesWrapWidget extends StatelessWidget {
  const PeculiaritiesWrapWidget({
    required this.peculiarities,
    super.key,
  });

  final List<String?> peculiarities;

  @override
  Widget build(BuildContext context) {
    if (peculiarities.isEmpty) {
      return const SizedBox.shrink();
    } else {
      final peculiaritiesWidgets = List.generate(
        peculiarities.length,
        (index) => PeculiaritiesItemWidget(peculiarity: peculiarities[index]),
      );
      return Wrap(
        spacing: 5,
        runSpacing: 8,
        children: peculiaritiesWidgets,
      );
    }
  }
}
