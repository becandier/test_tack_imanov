import 'package:flutter/material.dart';
import 'package:test_task_imanov/app/_app.dart';

class DefaultContainerWidget extends StatelessWidget {
  const DefaultContainerWidget({
    required this.child,
    this.padding,
    super.key,
  });
  final Widget child;
  final EdgeInsetsGeometry? padding;

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: context.appColors.white,
      ),
      child: Padding(
        padding: padding ?? const EdgeInsets.all(16),
        child: child,
      ),
    );
  }
}
