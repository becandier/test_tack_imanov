import 'dart:math';

import 'package:flutter/material.dart';

class CustomListTileWidget extends StatelessWidget {
  const CustomListTileWidget({
    required this.leading,
    required this.titleText,
    required this.subtitleText,
    this.onTap,
    this.trailing,
    super.key,
  });
  final VoidCallback? onTap;
  final Widget leading;
  final String? titleText;
  final String? subtitleText;
  final Widget? trailing;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTap,
      contentPadding: EdgeInsets.zero,
      title: Text(
        titleText ?? '',
        style: Theme.of(context).textTheme.labelLarge,
      ),
      subtitle: Text(
        subtitleText ?? '',
        style: Theme.of(context).textTheme.labelMedium!.copyWith(fontSize: 14),
      ),
      leading: leading,
      trailing: trailing ??
          Transform.rotate(
            angle: pi,
            child: const Icon(
              Icons.arrow_back_ios,
              size: 18,
            ),
          ),
    );
  }
}
