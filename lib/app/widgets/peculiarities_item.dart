import 'package:flutter/material.dart';
import 'package:test_task_imanov/app/_app.dart';

class PeculiaritiesItemWidget extends StatelessWidget {
  const PeculiaritiesItemWidget({
    required this.peculiarity,
    super.key,
  });

  final String? peculiarity;

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: ShapeDecoration(
        color: context.appColors.gray,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 5,
        ),
        child: Text(
          peculiarity ?? '',
          style: Theme.of(context).textTheme.labelMedium,
        ),
      ),
    );
  }
}
