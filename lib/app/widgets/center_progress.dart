import 'package:flutter/material.dart';

class CenterAdaptiveProgressIndicatorWidget extends StatelessWidget {
  const CenterAdaptiveProgressIndicatorWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }
}
