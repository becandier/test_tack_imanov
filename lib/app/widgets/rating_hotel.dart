import 'package:flutter/material.dart';
import 'package:test_task_imanov/app/_app.dart';

class RatingHotelWidget extends StatelessWidget {
  const RatingHotelWidget({
    required this.ratingNumber,
    required this.ratingName,
    super.key,
  });
  final int? ratingNumber;

  final String? ratingName;

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        color: context.appColors.yellow!.withOpacity(0.2),
        borderRadius: BorderRadius.circular(5),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(
              Icons.star,
              size: 22,
              color: context.appColors.yellow,
            ),
            const SizedBox(width: 2),
            Text(
              (ratingNumber ?? 0).toString(),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16,
                color: context.appColors.yellow,
              ),
            ),
            const SizedBox(width: 5),
            Text(
              ratingName ?? '',
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 16,
                color: context.appColors.yellow,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
