import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class CustomCashedNetworkImageWidget extends StatelessWidget {
  const CustomCashedNetworkImageWidget({
    required this.imgPath,
    super.key,
  });

  final String? imgPath;

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: imgPath ?? '',
      fit: BoxFit.fill,
      progressIndicatorBuilder: (context, url, downloadProgress) =>
          CircularProgressIndicator.adaptive(
        value: downloadProgress.progress,
      ),
      errorWidget: (context, url, error) => const Icon(Icons.error),
    );
  }
}
