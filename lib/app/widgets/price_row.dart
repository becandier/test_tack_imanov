import 'package:flutter/material.dart';

///Widget for price and price title
class PriceRowWidget extends StatelessWidget {
  ///Widget for price and price title

  const PriceRowWidget({
    required this.priceTitle,
    required this.priceText,
    super.key,
  });

  final String? priceText;
  final String? priceTitle;

  @override
  Widget build(BuildContext context) {
    return Row(
      textBaseline: TextBaseline.alphabetic,
      crossAxisAlignment: CrossAxisAlignment.baseline,
      children: [
        Text(
          priceText ?? '',
          style: Theme.of(context).textTheme.displayMedium,
        ),
        const SizedBox(width: 10),
        Text(
          priceTitle ?? '',
          style: Theme.of(context).textTheme.labelSmall,
        ),
      ],
    );
  }
}
