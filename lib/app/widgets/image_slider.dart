import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:test_task_imanov/app/widgets/_widgets.dart';

///Изображение слайдера
class ImageSliderWidget extends StatefulWidget {
  ///Изображение слайдера
  const ImageSliderWidget({
    required this.imagesPath,
    super.key,
  });

  final List<String?>? imagesPath;

  @override
  State<ImageSliderWidget> createState() => _ImageSliderWidgetState();
}

class _ImageSliderWidgetState extends State<ImageSliderWidget> {
  int? activeIndex;
  late int imageCount;
  late List<Widget> listimagesPath;
  @override
  void initState() {
    activeIndex = 0;
    imageCount = widget.imagesPath?.length ?? 0;
    listimagesPath = List.generate(
      widget.imagesPath?.length ?? 0,
      (index) {
        final img = widget.imagesPath?[index];
        return CustomCashedNetworkImageWidget(imgPath: img);
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 343 / 257,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: Stack(
          children: [
            CarouselSlider(
              options: CarouselOptions(
                enableInfiniteScroll: false,
                viewportFraction: 1,
                aspectRatio: 343 / 257,
                onPageChanged: (index, reason) {
                  setState(() {
                    activeIndex = index;
                  });
                },
              ),
              items: listimagesPath,
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: BulletsWidget(
                  activeIndex: activeIndex ?? 0,
                  diameter: 8,
                  count: imageCount,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
