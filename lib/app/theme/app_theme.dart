import 'package:flutter/material.dart';
import 'package:test_task_imanov/app/theme/_theme.dart';

class AppThemeDatas {
  /// Светлая тема приложения
  static ThemeData get lightTheme {
    final appColors = AppColors(
      white: LightColors.white,
      gray: LightColors.gray,
      primary: LightColors.primary,
      red: LightColors.red,
      orange: LightColors.orange,
      yellow: LightColors.yellow,
      black: LightColors.black,
      green: LightColors.green,
      textBlack: LightColors.textBlack,
      textDarkGray: LightColors.textDarkGray,
      textLink: LightColors.textLink,
    );

    return ThemeData(
      scaffoldBackgroundColor: LightColors.white.withOpacity(0.95),
      expansionTileTheme: ExpansionTileThemeData(
        shape: BeveledRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      brightness: Brightness.light,
      colorScheme: const ColorScheme.light().copyWith(
        primary: LightColors.primary,
      ),
      dividerTheme: DividerThemeData(
        color: LightColors.black.withOpacity(0.25),
      ),
      dividerColor: LightColors.gray,
      cupertinoOverrideTheme: MaterialBasedCupertinoThemeData(
        materialTheme: ThemeData(
          fontFamily: 'SF Pro Display',
        ),
      ),
      fontFamily: 'SF Pro Display',
      progressIndicatorTheme: const ProgressIndicatorThemeData(
        color: LightColors.primary,
      ),
      useMaterial3: true,
      textButtonTheme: const TextButtonThemeData(
        style: ButtonStyle(
          padding: MaterialStatePropertyAll(EdgeInsets.zero),
        ),
      ),
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      dialogTheme: const DialogTheme(surfaceTintColor: Colors.transparent),
      primaryColor: LightColors.primary,
      extensions: [
        appColors,
      ],
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          elevation: 0,
          foregroundColor: LightColors.white,
          backgroundColor: LightColors.primary,
          textStyle: const TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 16,
          ),
          surfaceTintColor: Colors.transparent,
          maximumSize: const Size(double.infinity, 60),
          minimumSize: const Size(double.infinity, 48),
          fixedSize: const Size(double.infinity, 48),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
        ),
      ),
      textSelectionTheme: const TextSelectionThemeData(
        cursorColor: LightColors.textBlack,
      ),
      inputDecorationTheme: InputDecorationTheme(
        isCollapsed: true,
        errorMaxLines: 2,
        labelStyle: LightTextStyles.labelSmall.copyWith(fontSize: 17),
        // labelStyle:
        //     AppTextStyles.body2_14.copyWith(color: LightColors.textBlack),
        // isDense: true,
        fillColor: LightColors.gray,
        filled: true,
        // hintStyle:
        //     AppTextStyles.body1_16.copyWith(color: LightColors.surfaceBlack),
        contentPadding: const EdgeInsets.symmetric(
          horizontal: 20,
          vertical: 10,
        ),

        enabledBorder: const OutlineInputBorder(
          borderSide: BorderSide(
            color: LightColors.gray,
          ),
        ),

        /// Обычное состояние
        border: const OutlineInputBorder(
          borderSide: BorderSide(
            color: LightColors.gray,
          ),
        ),

        /// Активное состояние
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(
            color: LightColors.gray,
          ),
        ),

        /// Ошибка
        errorBorder: const OutlineInputBorder(
          borderSide: BorderSide(
            color: LightColors.red,
          ),
        ),
        alignLabelWithHint: true,
      ),
      appBarTheme: const AppBarTheme(
        surfaceTintColor: Colors.transparent,
        elevation: 1,
        toolbarHeight: 60,
        titleTextStyle: TextStyle(
          fontWeight: FontWeight.w500,
          color: LightColors.black,
          fontSize: 20,
        ),
      ),
      textTheme: const TextTheme(
        titleLarge: LightTextStyles.titleLarge,
        titleMedium: LightTextStyles.titleMedium,
        labelLarge: LightTextStyles.labelLarge,
        labelMedium: LightTextStyles.labelMedium,
        labelSmall: LightTextStyles.labelSmall,
        displayMedium: LightTextStyles.displayMedium,
      ),
    );
  }
}
