import 'package:flutter/material.dart';
import 'package:test_task_imanov/app/_app.dart';

class DialogModals {
  static void showSuccesSnackBar(BuildContext context) {
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text('Супер!'),
      ),
    );
  }

  static void showErrorSnackBar(BuildContext context, {String? textError}) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        backgroundColor: context.appColors.red,
        content: Text(textError ?? 'Что-то пошло не так'),
      ),
    );
  }
}
