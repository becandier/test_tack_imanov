import 'package:flutter/material.dart';
import 'package:test_task_imanov/app/theme/colors/_colors.dart';

class LightTextStyles {
  /// === TEXT ===
  static const displayMedium = TextStyle(
    fontSize: 30,
    fontFamily: 'SF Pro Display',
    fontWeight: FontWeight.w600,
  );
  static const titleLarge = TextStyle(
    fontSize: 22,
    fontFamily: 'SF Pro Display',
    fontWeight: FontWeight.w500,
  );

  static const titleMedium = TextStyle(
    fontSize: 16,
    fontFamily: 'SF Pro Display',
    fontWeight: FontWeight.w400,
  );
  static const labelLarge = TextStyle(
    fontSize: 16,
    fontFamily: 'SF Pro Display',
    fontWeight: FontWeight.w500,
  );

  static const labelMedium = TextStyle(
    color: LightColors.textDarkGray,
    fontSize: 16,
    fontFamily: 'SF Pro Display',
    fontWeight: FontWeight.w500,
  );
  static const labelSmall = TextStyle(
    color: LightColors.textDarkGray,
    fontSize: 16,
    fontFamily: 'SF Pro Display',
    fontWeight: FontWeight.w400,
  );
}
