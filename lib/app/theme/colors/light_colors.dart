import 'package:flutter/material.dart';

class LightColors {
  /// =========== PRIMARY =============

  static const primary = Color(0xFF0D72FF);

  /// =========== WHITE ==============

  static const white = Color(0xFFFFFFFF);

  /// === RED ===
  static const red = Color(0xFFF70926);

  /// === DARK ===
  static const black = Color.fromRGBO(28, 29, 31, 1);
  static const gray = Color(0xFFF6F6F9);

  /// === GREEN ===
  static const green = Color(0xFF10C483);

  /// =========== TEXT COLORS ==============

  /// === DARK ===
  static const textBlack = Color(0xFF1C1D1F);
  static const textDarkGray = Color(0xFF828796);

  /// === BLUE ===
  static const textLink = Color(0xFF007AFF);

  /// === YELLOW ===
  static const yellow = Color(0xFFFFA800);

  /// === ORANGE ===
  static const orange = Color(0xFFFF6D00);
}
