// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

class AppColors extends ThemeExtension<AppColors> {
  AppColors({
    required this.gray,
    required this.primary,
    required this.white,
    required this.red,
    required this.orange,
    required this.yellow,
    required this.black,
    required this.green,
    required this.textBlack,
    required this.textDarkGray,
    required this.textLink,
  });

  /// =========== PRIMARY =============

  final Color? primary;

  /// =========== WHITE ==============

  final Color? white;

  /// === RED ===
  final Color? red;

  /// === DARK ===
  final Color? black;

  final Color? gray;

  /// === GREEN ===
  final Color? green;

  /// =========== TEXT COLORS ==============

  /// === DARK ===
  final Color? textBlack;
  final Color? textDarkGray;

  /// === BLUE ===
  final Color? textLink;

  /// === YELLOW ===
  final Color? yellow;

  /// === ORANGE ===
  final Color? orange;

  @override
  ThemeExtension<AppColors> lerp(ThemeExtension<AppColors>? other, double t) {
    if (other is! AppColors) {
      return this;
    }
    return AppColors(
      gray: Color.lerp(gray, other.gray, t),
      textBlack: Color.lerp(textBlack, other.textBlack, t),
      primary: Color.lerp(primary, other.primary, t),
      white: Color.lerp(white, other.white, t),
      black: Color.lerp(black, other.black, t),
      red: Color.lerp(red, other.red, t),
      yellow: Color.lerp(yellow, other.yellow, t),
      orange: Color.lerp(orange, other.orange, t),
      green: Color.lerp(green, other.green, t),
      textDarkGray: Color.lerp(textDarkGray, other.textDarkGray, t),
      textLink: Color.lerp(textLink, other.textLink, t),
    );
  }

  @override
  AppColors copyWith({
    Color? primary,
    Color? white,
    Color? red,
    Color? black,
    Color? gray,
    Color? green,
    Color? textBlack,
    Color? textDarkGray,
    Color? textLink,
    Color? yellow,
    Color? orange,
  }) {
    return AppColors(
      primary: primary ?? this.primary,
      white: white ?? this.white,
      red: red ?? this.red,
      black: black ?? this.black,
      gray: gray ?? this.gray,
      green: green ?? this.green,
      textBlack: textBlack ?? this.textBlack,
      textDarkGray: textDarkGray ?? this.textDarkGray,
      textLink: textLink ?? this.textLink,
      yellow: yellow ?? this.yellow,
      orange: orange ?? this.orange,
    );
  }
}

extension AppColorsX on BuildContext {
  AppColors get appColors => Theme.of(this).extension<AppColors>()!;
}
