import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_task_imanov/app/_app.dart';
import 'package:test_task_imanov/data/_data.dart';

class App extends StatelessWidget {
  const App({required this.mainRepository, super.key});

  final MainRepository mainRepository;

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider.value(
      value: mainRepository,
      child: const AppView(),
    );
  }
}

class AppView extends StatefulWidget {
  const AppView({super.key});

  @override
  State<AppView> createState() => _AppViewState();
}

class _AppViewState extends State<AppView> {
  late AppRouter appRouter;
  @override
  void initState() {
    appRouter = AppRouter();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'Test Task Imanov',
      theme: AppThemeDatas.lightTheme,
      routerConfig: appRouter.config(),
      builder: (context, child) {
        return GestureDetector(
          onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
          child: MediaQuery(
            data: MediaQuery.of(context).copyWith(
              // ignore: use_named_constants
              textScaler: const TextScaler.linear(0.95),
            ),
            child: child ?? const SizedBox(),
          ),
        );
      },
    );
  }
}
